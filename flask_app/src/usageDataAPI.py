from requests import get
from pandas import DataFrame
from json import loads as toJSON
from solarData import returnSolarDataRange

def returnUsageData(timeFrom='', timeTo='', userid='143908', apikey='569fac8e9f337861467a3298a06a2abc'):
    '''
        Returns the total value for accummulated kwh
    '''
    try:
        url = 'https://emoncms.org/feed/data.json?id=%s&start=%s&end=%s&interval=900&skipmissing=0&limitinterval=undefined&apikey=%s' %(userid, timeFrom, timeTo, apikey)
        site = get(url)
        if site.ok:
            if 'request end time before start time' in site.content:
                raise Exception('you included the request end time before the start time')
            df = DataFrame(toJSON(site.content), columns=['Time', 'Value'])
            df.dropna(inplace=True)
            times = df['Time']
            timesList = list(times)
            df.index = df['Time'].astype(str)
            # df.set_index('Time', inplace=True)
            t0 = str(min(timesList, key=lambda x:abs(x - int(timeFrom))))
            t1 = str(min(timesList, key=lambda x:abs(x - int(timeTo))))
            totalSolar = returnSolarDataRange(timeFrom[:10], timeTo[:10])
            return {
                'value': sum(list(df[t0:t1]['Value'])) - totalSolar['value'],
                'error': False
            }
        return {
            'value': False,
            'error': site.status_code
        }
    except KeyError:
        return {
            'value': 0.0,
            'error': 'out of scope'
        }
    except Exception as e:
        print 'error getting energy usage data: %s' %(e)
        return {
            'value': 0.0,
            'error': e
        }