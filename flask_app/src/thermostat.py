import nest
import sys

def get_temperature(nest_api):
    nest_api = initialise_nest()
    return return_values(get_nest_values(nest_api))

def set_temperature(target_temp, nest_api):
    nest_api = initialise_nest()
    structure = nest_api.structures[0]
    thermostat = structure.thermostats[0]

    change_mode('heat', nest_api, thermostat)
    thermostat.target = target_temp

    return return_values(get_nest_values(nest_api))

def change_mode(mode, nest_api, thermostat):
    if get_nest_values(nest_api)['mode'] != mode:
        thermostat.mode = mode

def initialise_nest():
    client_id = 'eneter-id-here'
    client_secret = 'enter-secret-here'
    access_token_cache_file = 'nest.json'

    napi = nest.Nest(client_id=client_id, client_secret=client_secret, access_token_cache_file=access_token_cache_file)

    if napi.authorization_required:
        print('Go to ' + napi.authorize_url + ' to authorize, then enter PIN below')
        if sys.version_info[0] < 3:
            pin = raw_input("PIN: ")
        else:
            pin = input("PIN: ")
        napi.request_token(pin)

    return napi

def return_values(nest_values):
    return {
        'error': False,
        'room_temp': nest_values['temperature'],
        'target_temp': nest_values['target'],
        'has_leaf': nest_values['has_leaf'],
        'is_away': nest_values['away'],
        'mode': nest_values['mode']
    }

def get_nest_values(nest_api):
    structure = nest_api.structures[0]
    away = is_away(structure.away)
    thermostat = structure.thermostats[0]

    return {
        'temperature': thermostat.temperature,
        'target': thermostat.target,
        'has_leaf': thermostat.has_leaf,
        'mode': thermostat.mode,
        'away': away
    }

def is_away(away):
    return True if away == 'away' else False

def turn_heat_off(nest_api):
    nest_api = initialise_nest()
    structure = nest_api.structures[0]
    thermostat = structure.thermostats[0]
    change_mode('eco', nest_api, thermostat)
    return return_values(get_nest_values(nest_api))

if __name__ == '__main__':
    napi = initialise_nest()
    print set_temperature(20, napi)
