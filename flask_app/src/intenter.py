from wit import Wit

def getIntent(accessToken='2TJL6KZJOZFSEO3SCIFLX4BZDYWF5XG3', message=''):
    '''
        Calls the Wit.ai API and returns the intent as dictionary

        Parameters
        ----------
        accessToken: str
            the access token for the wit.ai API
        message: str
            the message string

        Returns
        -------
        dict
            {
                'intents': str|False,
                'error': str|False
            }
    '''
    try:
        think = Wit(access_token= accessToken)
        res = think.message(message)
        maxLikelyHood = max(intent['confidence'] for intent in res['entities']['intent'])
        mostLikely = [intent['value'] for intent in res['entities']['intent'] if intent['confidence'] == maxLikelyHood]
        return {
            'intent': mostLikely[0],
            'error': False
        }
    except KeyError:
        return {
            'intent': False,
            'error': 'no intent found'
        }
    except Exception as e:
        return {
            'intent': False,
            'error': e
        }
