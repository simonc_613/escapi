from arrow import now
from solarData import returnSolarData

def getSolarEfficiency(maximum=10000, timestamp=''):
    '''
        Calculates the solar panel efficiency for a given timestamp
    '''
    if not timestamp:
        timestamp = now().timestamp
    kwhSolar = returnSolarData(timestamp)
    return {
        'efficiency': kwhSolar['value'] / maximum,
        'error': kwhSolar['error']
    }