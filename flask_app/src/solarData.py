from pandas import read_csv
from arrow import now, get
from os import getcwd
from itertools import chain

def returnSolarData(timestamp):
    '''
        Returns the value of the solar data for the timestamp
    '''
    try:
        timeObj = get(timestamp)
        day = timeObj.format('DD/MM/') + '15' # we only have 2015 data
        hours = hourRange(timeObj.format('HH:mm'))
        solarData = importSolarData()
        return {
            'value': float(solarData[hours][day]),
            'error': False
        }
    except Exception as e:
        print 'error getting solar data: %s' %(e)
        return {
            'value': 0.0,
            'error': e
        }

def returnSolarDataRange(timeFrom, timeTo):
    '''
        Returns the sum of values between from and to
    '''
    try:
        t0 = get(timeFrom)
        t1 = get(timeTo)
        day0 = t0.format('DD/MM/') + '15' # we only have 2015 data
        day1 = t1.format('DD/MM/') + '15' # we only have 2015 data
        hours0 = hourRange(t0.format('HH:mm'))
        hours1 = hourRange(t1.format('HH:mm'))
        solarData = importSolarData()
        tableSection = solarData[day0:day1]
        firstRow = [float(i) for i in tableSection.get_values()[0]]
        endRow = [float(i) for i in tableSection.get_values()[-1]]
        rest = list(chain.from_iterable(list(tableSection.get_values()[1:-1])))
        indexStart = list(tableSection.columns).index(hours0)
        indexEnd = list(tableSection.columns).index(hours1)
        final = firstRow[indexStart:] + rest + endRow[:-indexEnd]
        return {
            'value': sum(final),
            'error': False
        }
    except Exception as e:
        print 'error getting solar data range: %s' %(e)
        return {
            'value': 0.0,
            'error': e
        }

def importSolarData():
    '''
        Imports the solar panel data and returns as dataframe
    '''
    if 'flask_app' in getcwd():
        return read_csv(getcwd().split('flask_app').pop(0) + 'flask_app/src/data/hourlysolardata.csv', index_col=0)
    else:
        return read_csv(getcwd() + '/flask_app/src/data/hourlysolardata.csv', index_col=0)

def hourRange(tiempo):
    '''
        Returns the hour range in order to extract
        the data from the table
    '''
    broken = tiempo.split(':')
    hour = int(broken[0])
    if hour < 24:
        if len(str(hour)) < 2:
            if len(str(hour+1)) < 2:
                return '0%s:00-0%s:00' %(hour, hour+1)
            return '0%s:00-%s:00' %(hour, hour+1)
        return '%s:00-%s:00' %(hour, hour+1)
    return '00:00-01:00'

# debugging
# print returnSolarData(str(get('2015-05-01 12:00').timestamp))