from solarData import returnSolarData
from arrow import now

def appShouldNotify(wattage):
    '''
        Assess whether the app needs to notify the user to 
        switch on devices due to optimum pv energy supply
    '''
    threshold = 500
    if wattage:
    	threshold = wattage
    timestamp = now().timestamp
    kwhSolar = returnSolarData(timestamp)
    if kwhSolar['value'] > threshold:
        return True
    return False
    