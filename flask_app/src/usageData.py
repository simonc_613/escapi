from pandas import read_csv
from arrow import now, get
from os import getcwd

def returnUsageData(timeFrom, timeTo):
    '''
        Returns the total value for accummulated kwh
    '''
    try:
        timeFrom = get(timeFrom)
        timeTo = get(timeTo)
        timeFromKey = closestQuarter(timeFrom)
        timeToKey = closestQuarter(timeTo)
        if 'flask_app' in getcwd():
            usageData = read_csv(getcwd().split('flask_app').pop(0) + 'flask_app/src/data/historicalUsage.csv', index_col=0)
        else:
            usageData = read_csv(getcwd() + '/flask_app/src/data/historicalUsage.csv', index_col=0)
        return {
            'value': sum(list(usageData[timeFromKey:timeToKey]['kwh'])),
            'error': False
        }
    except KeyError:
        return {
            'value': 0.0,
            'error': 'out of scope'
        }
    except Exception as e:
        print 'error getting solar data: %s' %(e)
        return {
            'value': 0.0,
            'error': e
        }

def closestQuarter(timestamp):
    tiempo = get(timestamp)
    day = tiempo.format('YYYY-MM-DD')
    hour = tiempo.format('HH')
    minute = tiempo.format('mm')
    ranges = {
        '00': range(0, 15),
        '15': range(15, 30),
        '30': range(30, 45),
        '45': range(45, 60)
    }
    closest = '00'
    for quarter, sequence in ranges.items():
        if minute in sequence:
            closest = quarter
            break
    return '%s %s:%s:00' %(day, hour, closest)