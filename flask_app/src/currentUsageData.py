from requests import get
from json import loads as toJSON

def returnCurrentUsage(userid='143908', apikey='569fac8e9f337861467a3298a06a2abc'):
	url = 'https://emoncms.org/feed/timevalue.json?apikey=%s&id=%s' %(apikey, userid)
	site = get(url)
	if site.ok:
		data = toJSON(site.content)
		return {
			'value': data['value'],
			'time': data['time'],
			'error': False
		}
	return {
		'value': False,
		'error': site.status_code
	}