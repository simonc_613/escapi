from flask import Flask, request, json
from json import loads as toJSON
from src.intenter import getIntent
from src.solarEfficiency import getSolarEfficiency
from src.solarData import returnSolarData
from src.usageDataAPI import returnUsageData
from src.currentUsageData import returnCurrentUsage
from src.thermostat import get_temperature, set_temperature, initialise_nest, turn_heat_off
from src.optimumSolarOutput import appShouldNotify

app = Flask(__name__)
nest_api = initialise_nest()

@app.route('/')
def hello_world():
    return "Hello, world!"

@app.route('/getintent', methods=['POST'])
def returnIntent():
    '''
        Returns the intent after querying wit
    '''
    dataTypes = {
        'form': request.form.to_dict(),
        'json': request.get_json()
    }
    if not any(value for value in dataTypes.values()):
        try:
            dataTypes['string'] = toJSON(request.data)
        except Exception as e:
            return e, 400
    for key, data in dataTypes.items():
        if data:
            if data['type'] == 'voice_command':
                intent = getIntent(message=data['text'])
                response = app.response_class(
                    response=json.dumps(intent),
                    status=200,
                    mimetype='application/json'
                )
                return response
    return 'ok', 200

@app.route('/iot/thermostat', methods=['GET', 'POST'])
def thermostat():
    if request.method == 'GET':
        return app.response_class(
            response=json.dumps(get_temperature(nest_api=nest_api)),
            status=200,
            mimetype='application/json'
        )
    else:
        return app.response_class(
            response=json.dumps(set_temperature(request.get_json()['target_temp'], nest_api=nest_api)),
            status=200,
            mimetype='application/json'
        )
        return 'no data provided', 400

@app.route('/iot/thermostat/off', methods=['GET'])
def heat_off():
    return app.response_class(
        response=json.dumps(turn_heat_off(nest_api)),
        status=200,
        mimetype='application/json'
    )

@app.route('/pv/efficiency', methods=['GET'])
def returnData():
    '''
        Returns the current solar panel efficiency
    '''
    timestamp = request.args.get('timestamp')[:10]
    response = app.response_class(
        response=json.dumps(getSolarEfficiency(timestamp=timestamp)),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/pv/usage', methods=['GET'])
def returnSolarUsage():
    '''
        Returns the value for the solar usage given the timestamp
    '''
    timestamp = request.args.get('timestamp')[:10]
    response = app.response_class(
        response=json.dumps(returnSolarData(timestamp)),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/notification', methods=['GET'])
def shouldNotify():
    '''
        Returns object to tell app whether it should notify user
    '''
    wattage = request.args.get('wattage')
    if appShouldNotify(wattage):
        return app.response_class(
            response=json.dumps({'notify': True}),
            status=200,
            mimetype='application/json'
        )
    return app.response_class(
        response=json.dumps({'notify': False}),
        status=200,
        mimetype='application/json'
    )

@app.route('/energy/current', methods=['GET'])
def returnCurrentUsageData():
    '''
        Returns current energy consumption
    '''
    userid = request.args.get('id')
    apikey = request.args.get('apikey')
    if not userid and not apikey:
        final = json.dumps(returnCurrentUsage())
    else:
        final = json.dumps(returnCurrentUsage(userid=userid, apikey=apikey))
    return app.response_class(
        response=final,
        status=200,
        mimetype='application/json'
    )

@app.route('/energy/usage', methods=['GET'])
def returnEnergyUsage():
    '''
        Returns total usage between two timestamps
    '''
    try:
        t0 = request.args.get('from')
        t1 = request.args.get('to')
        if not t0 and not t1:
            return 'You need to include "from" and "to" URL parameters as timestamps', 400
        response = app.response_class(
            response=json.dumps(returnUsageData(timeFrom=t0, timeTo=t1)),
            status=200,
            mimetype='application/json'
        )
        return response
    except Exception as e:
        return e, 400

if __name__ == '__main__':
    app.run(debug=False)