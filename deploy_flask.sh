#!/usr/bin/env bash

cd /opt/escapi
git pull

#restart flask
kill -9 $(pgrep flask)
./run-flask-mac.sh &
exit